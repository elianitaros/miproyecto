<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntecedenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antecendente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alergia',25);
            $table->string('grupo_sanguineo',10);
            $table->string('rh',10);
            $table->integer('antheredfamiliar_id');
            $table->foreign('antheredfamiliar_id')->references('id')->on('antheredfamiliar')->onDelete('cascade');
            $table->integer('antpersonal_id');
            $table->foreign('antpersonal_id')->references('id')->on('antpersonal')->onDelete('cascade');
            $table->integer('antperinatal_id');
            $table->foreign('antperinatal_id')->references('id')->on('antperinatal')->onDelete('cascade');
            $table->integer('antgineco_id');
            $table->foreign('antgineco_id')->references('id')->on('antgineco')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antecendente');
    }
}
