<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntperinatalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antperinatal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero_hijo');
            $table->integer('meses_gestacion');
            $table->enum('sitio_nac',['domicilio','hospital','c/s puesto de salud','otros']);
            $table->string('descripcion',20)->nullable();
            $table->string('tipo_nac',15);
            $table->float('peso');
            $table->float('talla');
            $table->enum('problemas_nac',['si','no']);
            $table->string('especificacion',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antperinatal');
    }
}
