<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntheredfamiliarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antheredfamiliar', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('cardiovascular',['ninguno','hipertension arterial','cardiopatia isquemica','otros']);
            $table->string('descripcion',30)->nullable();
            $table->enum('endrocrino',['ninguno','diabetes mellitus','enfi. tiroideas','otros']);
            $table->string('descripcion1',30)->nullable();
            $table->enum('neurlogico',['ninguno','migraña','epilepsia','enf. depresivas','otros']);
            $table->string('descripcion3',30)->nullable();
            $table->enum('respiratorio',['ninguno','asma','tbc','otros']);
            $table->string('descripcion4',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antheredfamiliar');
    }
}
