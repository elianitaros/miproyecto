<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreatePacientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',25);
            $table->string('surname',25);
            $table->string('lastname',25)->nullable;
            $table->string('address',50);
            $table->date('birthdate','created_at');
            $table->integer('age');
            $table->enum('sex',['Femenino','Masculino']);
            $table->string('job',30);
            $table->string('origin',30);
            $table->integer('telephone')->nullable;
            $table->integer('cellphone');
            $table->enum('civilstatus',['soltero(a)','casado(a)','viudo(a)','union libre']);
            $table->string('emergencycase',100);
            $table->integer('optionalphone');
            $table->string('image',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pacients');
    }
}
