<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntginecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antgineco', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('aplica',['no' ,'si']);
            $table->string('menarca',20);
            $table->string('telarca',20);
            $table->string('ritmo_menstrual',20);
            $table->enum('dismenorrea',['si','no']);
            $table->string('fum',10);
            $table->enum('metodo_planificacion',['si','no']);
            $table->string('descripcion',20)->nullable();
            $table->enum('pap',['si','no']);
            $table->string('descripcion1',20)->nullable();
            $table->enum('mamografia',['si','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antgineco');
    }
}
