<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('turno',['mañana','tarde','noche']);
            $table->integer('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('medical_speciality_id')->nullable();
            $table->foreign('medical_speciality_id')->references('id')->on('medical_speciality')->onDelete('cascade');
            $table->dateTime('started_at');
            $table->dateTime('finished_at');
            $table->dateTime('hora_i','created_at');
            $table->dateTime('hora_f','created_at');
            $table->enum('tiempo_consulta',['10','15','20','25']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('horario');
    }
}

