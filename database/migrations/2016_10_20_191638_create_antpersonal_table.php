<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntpersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antpersonal', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('a',['ninguno','quirurgicos','transfucionales','traumaticos','hospitalizaciones previas','otros']);
            $table->string('descripcion',30)->nullable();
            $table->enum('tabaquismo',['si','no','a veces']);
            $table->enum('alcohol',['si','no','a veces']);
            $table->enum('drogas',['si','no','aveces']);
            $table->enum('inmunizaciones',['completas a edad','pendientes','otros']);
            $table->string('descripcion1',30)->nullable();;
            $table->enum('enfermedad_infecciosa',['ninguno','parotiditis','rubeola','sarampion','varicela','hepatitis','fiebre tifiodea','otros']);            
            $table->string('descripcion2',30)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antpersonal');
    }
}
