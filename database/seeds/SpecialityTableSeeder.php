<?php

use Illuminate\Database\Seeder;
use App\Speciality;

class SpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(\App\Speciality::class)->times(50)->create();
    }
}
