<?php

use Illuminate\Database\Seeder;

use App\People;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $peoples = People::all();
        $users = factory(User::class)->times(50)->make();
        $var = 0;

        
        
            while($var < count($peoples))
            {
                $people = $peoples[$var];
                $people->user()->save($users[$var]);
                $var++;
            }
        }
}


