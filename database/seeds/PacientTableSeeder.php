<?php

use Illuminate\Database\Seeder;
use Faker\Factory as faker;

class PacientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');
        for ($i=0; $i<50; $i++)
        {
        	\DB::table('pacients')->insert(array(
        		'name' 		=> $faker->firstName,
        		'surname' 	=> $faker->lastName,
        		'lastname' 	=> $faker->lastName,
        		'address'	=> $faker->streetAddress,
        		'birthdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        		'age' 		=> $faker->numberBetween($min = 1, $max = 2),
        		'sex' 		=> $faker->randomElement(['Femenino','Masculino']),
        		'job'		=> $faker->jobTitle,
        		'origin'	=> $faker->city,
        		'telephone'	=> $faker->randomNumber,
        		'cellphone'	=>	$faker->randomNumber($nbDigits = NULL),
        		'civilstatus'=>	$faker->randomElement(['soltero(a)','casado(a),','viudo(a)','union libre']),
        		'emergencycase'	=>	$faker->name,
        		'optionalphone'	=>	$faker->randomNumber,
        		'image'			=>	$faker->imageUrl($width = 640, $height = 480), 
        		'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
        		));
        }
    }
}
