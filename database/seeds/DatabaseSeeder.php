<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SpecialityTableSeeder::class);
       // $this->call(PeopleTableSeeder::class);

       // $this->call(UserTableSeeder::class);
        
    }
}
