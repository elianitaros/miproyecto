<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
   protected $table= 'horario';

    protected $fillable = [
        'turno','dia','users_id','medical_speciality_id','hora_i','hora_f','tiempo_consulta'
    ];

    public function user()
    {
    	return $this->hasOne(User::class);
    }
    public function especialidad()
    {
    	return $this->hasOne(Speciality::class);
    }
}


