<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function people()
    {
        return $this->hasOne('App\people', 'id', 'people_id');
    }
    #establecemos la relaion con el modelo Role 

    public function admin()
    {
        return $this->type === "admin";
    }
    public function medical_speciality()
    {
        return $this->hasOne(Speciality::class);
    } 

}
