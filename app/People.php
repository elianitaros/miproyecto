<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class People
 * @package App
 * 
 * @property mixed name
 * @property mixed firstname
 * @property mixed lastname
 *
 */
class People extends Model
{
    /**
     * @var string
     */
    protected $table = 'people';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'firstname', 'lastname'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
