<?php

namespace App;
use People;
use User;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
	
    protected $table='medical_speciality'; 
    protected $fillable = [
        'name'
    ];
    public function people()
    {
        return $this->belongsTo(People::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
