<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacient extends Model
{
    protected $table = 'pacients';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
         'surname', 
         'lastname',
         'address',
         'birthdate',
         'age',
         'sex',
         'origin',
         'telephone',
         'cellphone','civilstatus','emergycase','optionalphone'
    ];
}
