<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PacientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [    
            'name'  =>  'required',
            'suername'  =>  'required',
            'address'   =>  'required',
            'birthdate' =>  'required',
            'sex'   =>  'required',
            'job'   =>  'required',
            'origin'    =>  'required',
            'cellphone' =>  'required|numeric',
            'civilstatus'   =>  'required',
            'emergencycase' =>  'required',
            'optionalphone'  =>  'required',
            'image'    =>  'required',
        ];
    }

    public function messages()
    {
        return[
            'name.required'         =>  'este campo es requerido!',
            'surname.required'      =>  'este campo es requerido!',
            'lastname.required'     =>  'este campo es requerido!',
            'address.required'      =>  'este campo es requerido!',
            'sex.required'          =>  'este campo es requerido!',
            'job.required'          =>  'este campo es requerido!',
            'origin.required'       =>  'este campo es requerido!',
            'cellphone.required'    =>  'este campo es requerido y solo puede contener numeros!',
            'civilstatus'           =>  'este campo es requerido!',
            'emergencycase'         =>  'este campo es requerido!',
            'optionalphone'         =>  'este campo es requerido y solo puede contener numeros!',
            'image'                 =>  'este campo es requerido!'

        ];
    }
}
