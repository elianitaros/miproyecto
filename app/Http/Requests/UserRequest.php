<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required|min:2|max:75',
            'firstname'     =>  'required|min:3|max:35',
            'lastname'      =>  'max|35',
            'username'      =>  'required|unique:users|min:5|max:25',
            'email'         =>  'required|unique:users|max:100',
            'password'      =>  'required|min:8|max:30',
            'type'          =>  'required'
        ];
    }
}
