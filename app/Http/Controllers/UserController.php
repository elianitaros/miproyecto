<?php

namespace App\Http\Controllers;

use App\People;
use App\User;
use Illuminate\Http\Request;
use App\Speciality;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UserRequest;

use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('people')->paginate(14);
        //$users = DB::table('users')->join('people', 'users.people_id', '=', 'people.id')
          //  ->select('users.*', 'people.*')
           // ->paginate(14);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $especialidades=Speciality::all();
        $people = new People;
        $people->name       =   $request->name;
        $people->firstname  =   $request->firstname;
        $people->lastname   =   $request->lastname;
        if($people->save())
        {
            $user = new User;
            $user->username =   $request->username;
            $user->email    =   $request->email;
            $user->password =   bcrypt($request->password);
            $user->type     =   $request->type;
            //$user->medical_speciality_id = null;

            if($user->type =='medico')
            {
                $user->medical_speciality_id=$request['medical_speciality_id'];
            }
            
            
           
            if($people->user()->save($user)){
                   

                Session::flash('msg_title', 'Exito');
                Session::flash('msg_description', 'se ha creado un nuevo usuario');
                Session::flash('msg_type', 'success');
            }
            else{
                Session::flash('msg_title', 'error');
                Session::flash('msg_description', 'los sentimos no pudo registrase un nuevo usuario');
                Session::flash('msg_type', 'danger');
            }
        }

        return redirect('users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // dd($id->id);
        /*$users = DB::table('users')
            ->join('people', 'users.id', '=', 'people.id')
            ->select('users.*', 'people.*')
            ->get();

        return view('user.show',  compact('users'));
        
        $users = User::find($id)
                        ->with('people');
        return view('user.show', compact('users'));






        $users = DB::table('people')->find($id)
            ->join('users', 'people.id', '=', 'users.people_id')
            ->select('people.*', 'users.*')
            ->get();
        return view('user.show', compact('users')); */


        $users = $id;
        return view('user.show',compact('users'));
       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->people;

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
