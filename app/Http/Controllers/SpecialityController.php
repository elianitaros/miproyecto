<?php

namespace App\Http\Controllers;
use App\Speciality;
use App\People;
use App\User;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;



class SpecialityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
            

            /*$u=DB::table('users')
                ->join('people', function($join)
                {
                    $join->on('users.id','=','people.id')
                    ->where('users.type','=','medic');
                })
                ->get();
            $speciality = pg_insert(connection, table_name, assoc_array)
            return ($u);
            $especialidades = Speciality::all();
        return view('especialidad.index', compact('especialidades'));


            */
        
        $especialidades = Speciality::orderBy('id','ASC')->paginate(10);
        return view('especialidad.index',compact('especialidades', $especialidades));
        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('especialidad.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'  => 'required'
            ]);
        Speciality::create($request->all());
        Session::flash('msg_title', 'Exito!!!');
        Session::flash('msg_description', 'se ha registrado una nueva especialidad');
        Session::flash('msg_type', 'success');

        return redirect()->route('especialidad.index');

        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $especialidad = Speciality::find($id);
        return view('especialidad.show',compact('especialidad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $especialidad = Speciality::find($id);
        return view('especialidad.edit',compact('especialidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
                'name' => 'required'
            ]);
        Speciality::find($id)->update($request->all());
        Session::flash('msg_title', 'Exito!!!');
        Session::flash('msg_description', 'especialidad actualizada');
        Session::flash('msg_type', 'success');

        return redirect()->route('especialidad.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Speciality::find($id)->delete($id);
        return redirect()->route('especialidad.index');
        Session::flash('msg_title', 'Exito!!!');
        Session::flash('msg_description', 'se elimino la especialidad');
        Session::flash('msg_type', 'success');

    }
    public function dame_especialidades()
    {
                    /*
                if (Request::ajax())
                {
                     $especialidades=Speciality::all();
                     for ($i=0; $i <count($especialidades) ; $i++)
                     { 
                         $datos[$i]=$especialidades[$i]->name;
                     }
                     echo json_encode($datos);
                    
                }
                else
                {
                    */
                     $result = array(); 
                     $especialidades=Speciality::all();
                     for ($i=0; $i <count($especialidades) ; $i++)
                     { 
                         $result[] = array(
                            "id" => $especialidades[$i]->id,
                            "value" => $especialidades[$i]->name
                            );
                     }
                     echo json_encode($result);
               // }        
     }
     public function mostrar_id()
     {
         $result = array(); 
                     $especialidades=Speciality::all();
                     for ($i=0; $i <count($especialidades) ; $i++)
                     { 
                         $result[] = array(
                            "id" => $especialidades[$i]->id,
                            
                            );
                     }
                     echo json_encode($result);
     }
}
