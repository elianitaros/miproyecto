<?php

namespace App\Http\Controllers;
use App\Pacient;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\PacientRequest;
use Carbon\Carbon;


class PacientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              
        $pacient = Pacient::orderBy('id','ASC')->paginate(10);
        return view('pacient.index')->with('pacient', $pacient);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('pacient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            /*$pacient = new Pacient;
            $pacient->name =$request->input('name');
            $pacient->surname = $request->input('surname');
            $pacient->lastname = $request->input('lastname');
            $pacient->address = $request->input('address');
            $pacient->birthdate = $request->input('birthdate');
            
            $pacient->age = Carbon::createFromDate(1975, 5, 21)->age;
            $pacient->sex = $request->input('sex');
            $pacient->job = $request->input('job');
            $pacient->origin = $request->input('origin');
            $pacient->telephone =$request->input('telephone');
            $pacient->cellphone = $request->input('cellphone');
            $pacient->civilstatus = $request->input('civilstatus');
            $pacient->emergencycase = $request->input('emergencycase');
            $pacient->optionalphone = $request->input('optionalphone');
            $pacient->image = $request->input('image');
            $pacient->save();
            return redirect('pacient');*/

            

            $pacient = Carbon::createFromDate($year ,$month,$day);
            
            //$age = Carbon::createFromDate($pacient)->age;
            dd($pacient);
            

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paci = Pacient::find($id);
        return ($paci);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
