<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/d', 'PacientController@store');
/*
Route::post('/test/save', ['as' => 'save-date',
                           'uses' => 'DateController@showDate', 
                            function () {
                                return '';
                            }]);
*/

/* 
Route::get('/test/datepicker', function () {
    return view('datepicker');
});
*/
 
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|

*/
/*Route::get('prueba',function()
    {
        $u=App\User::find(6)->admin();
        dd($u);

    });

Route::group(['middleware' => ['web','admin']], function () {
    //
});*/



//Route::get('specialty/j','SpecialityController@mostrar_id');
Route::group(['middleware' => ['web']], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

    Route::model('users', App\User::class);
    Route::resource('users', 'UserController');
});

Route::resource('pacient','PacientController');

Route::get('a','UserController@show');

;

//Route::get('specialty/j','SpecialityController@mostrar_id');

Route::get('cita', function()
    {
        return view('citamedica.index');
    });
Route::get('cita/create', function()
    {
        return view('citamedica.create');
    });

Route::get('horario/create', function()
    {
        return view('horario.create');
    });



Route::get('especialidad/json','SpecialityController@dame_especialidades');
Route::group(['middleware' => ['web']], function () {
    Route::resource('especialidad','SpecialityController');

    
});


