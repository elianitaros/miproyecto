<!DOCTYPE html>
<html lang="en">
<head>

    

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    {!! Html::style('assets/css/bootstrap.min.css') !!}
    
    <style>

        body {
            font-family: 'Lato';
            vertical-align: center;


        }

        .fa-btn {
            margin-right: 10px;
        }
        nav.navbar.navbar-default
        {
            
            
            background: #A9CBEB;
            height: 200px; 
            vertical-align: center;
        
        }
        .navbar-inverse
        {
            
            position: relative;
            
            top: 75px;
        }
     
       
        
    </style>
     
</head>
<body id="app-layout">

    <nav class="navbar navbar-default">

        <div class="container">
            
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
               
            </div>
            
    

        <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            
            <div class="navbar-header">
              <a class="navbar-brand" href="#">WebSiteName</a>
            </div>
            <ul class="nav navbar-nav">
              <li ><a href="{{ url('/')}}"><span class="glyphicon glyphicon-home"> INICIO</a></li> 
              <li><a href="#">ACERCA DE</a></li>
              <li><a href="{{ url('/home') }}">MODULOS DEL SISTEMA</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                  <li><a href="{{ url('/register') }}"><span class="glyphicon glyphicon-user"></span>  REGISTRATE</a></li>
                  <li><a href="{{ url('/login')}}"><span class="glyphicon glyphicon-log-in"></span>  INICIAR</a></li>
                @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->username }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>SALIR</a></li>
                            </ul>
                        </li>
                    @endif
            </ul>
          </div>
        </nav>

   
         

    </nav>

    @yield('content')
    {!! Html::script('assets/js/jquery.js') !!}
    {!! Html::script('assets/js/bootstrap.min.js') !!}


  

       
   <script>
        $(document).ready(function()
        {
               $("#type").change(function(){
                    var valor = $("#type").val();
                    if(valor == "medico")
                    {
                        var ur="/especialidad/json";
                           $.ajax({
                              type:"GET",
                              url:ur,
                              dataType:"json",
                              success:function(data){
                                   var myselect = '<select name="medical_speciality_id" id="medical_speciality_id">'; 
                                   for (var i = data.length - 1; i >= 0; i--) {
                                        myselect = myselect + '<option value="'+data[i].id+'">'+data[i].value+'</option>';                                   };
                                   myselect += '</select>';
                                   $("#myajax").empty();
                                   $("#myajax").append(myselect);                       
                                  
                              }

                           });

                    }
                    else
                      $("#myajax").empty();
               });
        });
</script>




<script type="text/javascript" src="/datePicker/js/bootstrap-datepicker.min.js"></script>
</body>
</html>
