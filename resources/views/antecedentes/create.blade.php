@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'antecedente.store', 'method' => 'POST', 'role' => 'form']) !!}
                <div class="form-group">
                    {{ Form::label('name', 'NOMBRES', [ 'for' =>'name']) }}
                    {{ Form::text('name', null, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('surname', 'apellido paterno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('surname', null, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('lastname', 'apellido materno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('lastname', null, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('address', 'direccion', ['class' => 'text-uppercase']) }}
                    {{ Form::text('address', null, ['class' => 'form-control text-uppercase']) }}
                </div>
                
                <div>
                    {{Form::label('birthdate','fecha nac',['for'=>'birthdate'])}}
                    <div class="container">
                            <div class="content">
                         
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-4 col-md-offset-4">
                         
                                            <form action="/test/save" method="post">
                                                <div class="form-group">

                                                    
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" name="date">
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-th"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </form>
                         
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $('.datepicker').datepicker({
                                format: "yyyy/mm/dd",
                                language: "es",
                                autoclose: true
                            });
                        </script>
                </div>
                    
               
                <div class="form-group">
                    {{ Form::label('age', 'Edad', ['class' => 'text-uppercase']) }}
                    {{ Form::text('age', null, ['class' => 'form-control text-uppercase']) }}
                </div>

                <div class="form-group">
                    {!! Form::label('sex', 'SEXO', ['for' => 'sex'] , ['class' => 'text-uppercase']  ) !!}
                    <div class="form-group">            
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='femenino'> Femenino
                            </label>
                        </div>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='masculino'> Masculino
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('job', 'OCUPACION', ['class' => 'text-uppercase']) }}
                    {{ Form::text('job' , null,  ['class' => 'form-control text-uppercase']) }}
                </div>
                
                <div class="form-group">
                    {{ Form::label('origin', 'procedencia', ['class' => 'text-uppercase']) }}
                    {{ Form::text('origin', null, ['class' => 'form-control text-uppercase']) }}
                </div>
                
                <div class="form-group">
                    {{ Form::label('telephone', 'telefono', ['class' => 'text-uppercase']) }}
                    {{ Form::text('telephone', null , ['class' => 'form-control text-uppercase']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('cellphone', 'celular', ['class' => 'text-uppercase']) }}
                    {{ Form::text('cellphone', null , ['class' => 'form-control text-uppercase']) }}
                </div>
                
                <div class="form-group">
                    {!! Form::label('civilstatus', 'ESTADO CIVIL', ['for' => 'civilstatus'] ) !!}
                    <div class="form-group">            
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='soltero(a)'> Soltero(a)
                            </label>
                        </div>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='casado(a)'> Casado(a)
                            </label>
                        </div>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='viudo(a)'> Viudo(a)
                            </label>
                        </div>
                        <div class="col-xs-2">
                            <label class="radio-inline">
                                <input type="radio" name="genderRadios" value='union libre'> Union Libre
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('emergencycase', 'caso de emergencia', ['class' => 'text-uppercase']) }}
                    {{ Form::text('emergencycase', null , ['class' => 'form-control text-uppercase']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('optionalphone', 'fono emergencia', ['class' => 'text-uppercase']) }}
                    {{ Form::text('optionalphone', null , ['class' => 'form-control text-uppercase']) }}
                </div>

                <div class="form-group">
                            {!! form::label('image','Imagen')!!}
                            {!! form::file('image',null,['class' => 'form-control']) !!}
                </div>

            
                <div class="form-group">
                    <a class="btn btn-xs btn-danger" href="{{ url('users') }}">CANCELAR</a>
                    <button class="btn btn-success btn-xs">GUARDAR</button>
                </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
