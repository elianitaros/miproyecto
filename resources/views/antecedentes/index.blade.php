@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary btn-xs" href="{{ url('especialidad/create') }}">NUEVA ESPECIALIDAD</a>

            @include('partials.message')

            
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>NOMBRE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($especialidad as $esp)
                        <tr>
                            <td>{{ $esp->name }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('speciality.show', ['id' => $spe->id]) }}" title="Editar usuario">Ver</a>
                                <a class="btn btn-warning btn-xs" href="{{ route('speciality.edit', ['id' => $spe->id]) }}" title="Editar usuario">Editar</a>
                                <a class="btn btn-danger btn-xs" href="{{ route('speciality.destroy', ['id' => spe->id]) }}" title="Eliminar usuario">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $speciality->links() !!}
            @else
                <div class="alert alert-info">
                    <h4>INFORMACION</h4>no existe ninguna especialidad creada
                </div>
            @endif
            
        </div>
    </div>
</div>
@endsection