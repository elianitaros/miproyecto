@extends('layouts.app')

@section('content')

<div class="container">

            
    <div>
    
       
        </thead>
        </table>

            <a class="btn btn-primary btn-xs" href="{{ url('pacient/create') }}">NUEVO PACIENTE</a>
            @include('partials.message')
            @if(count($pacient) > 0)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nombre y Apellido</th>
                        <th>direccion</th>
                        <th>fecha de nacimiento</th>
                        <th>edad</th>
                        <th>sexo</th>
                        <th>telf</th>
                        <th>cell</th>
                        <th>imagen</th>                     
                    </tr>
                </thead>
                <tbody>
                    @foreach($pacient as $pac)
                        <tr>
                            <td>{{ $pac->id }}</td>
                            <td>{{ $pac->name }} {{ $pac->surname }} {{ $pac->lastname }} </td>
                            <td>{{ $pac->address }}</td>
                            <td>{{ $pac->birthdate }}</td>
                            <td>{{ $pac->civilstatus }}</td>
                            <td>{{ $pac->age }}</td>
                            <td>{{ $pac->sex }}</td>
                            <td>{{ $pac->telephone }}</td>
                            <td>{{ $pac->cellphone }}</td>
                            <td>{{ $pac->image }}</td>           
                        </tr>
                        
                     @endforeach
                </tbody>
            </table>
            @endif
        </div>
        {!! $pacient->render() !!}
    </div>

</div>

@endsection

