@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
           
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('especialidad.index') }}"> ATRAS</a>
                    </div>
                </div>
            </div>


            <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>numero:</strong>
                            {{ $especialidad->id }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>nombre:</strong>
                            {{ $especialidad->name }}
                        </div>
                    </div>

                </div>

@endsection


