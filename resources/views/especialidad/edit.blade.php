@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
           
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('especialidad.index') }}"> ATRAS</a>
                    </div>
                </div>
            </div>
            
            @if(count($errors)>0)
            <div class="alert alert-danger">
                <strong>lo sentimos!!!</strong>ha ocurrido un error en la editacion.<br><br>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                </ul>
            </div>
            @endif


            {!! Form::model($especialidad, ['method' => 'PATCH','route' => ['especialidad.update', $especialidad->id]]) !!}
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>nombre:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'nombres','class' => 'form-control')) !!}
                        </div>
                    </div>

                    
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
 

@endsection
