@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'especialidad.store', 'method' => 'post', 'role' => 'form']) !!}

            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Crear una nueva especialidad</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('especialidad.index') }}"> ATRAS</a>
                    </div>
                </div>
            </div>



            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>error!</strong> usted debe ingresar una especialidad.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
           
                <div class="form-group">
                    {{ Form::label('name', 'nombre', ['class' => 'text-uppercase']) }}
                    {{ Form::text('name', null , ['class' => 'form-control text-uppercase']) }}
                   
                </div>
                
                <div class="form-group">
                    <a class="btn btn-xs btn-danger" href="{{ url('users') }}">CANCELAR</a>
                    <button class="btn btn-success btn-xs">GUARDAR</button>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection


