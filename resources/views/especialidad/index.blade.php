@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary btn-xs" href="{{ url('especialidad/create') }}">NUEVA ESPECIALIDAD</a>


             @include('partials.message')

            @if(count($especialidades) > 0)
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>NOMBRE</th>
                        <th>OPCIONES</th>                          
                    </tr>


                    
                </thead>
            
                <tbody>
                    @foreach($especialidades as $esp)
                        <tr>
                            <td>{{ $esp->id }}</td>
                            <td>{{ $esp->name }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('especialidad.show',$esp->id) }}">Ver</a>
                                <a class="btn btn-primary" href="{{ route('especialidad.edit',$esp->id) }}">Editar</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['especialidad.destroy', $esp->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                           
                        </tr>


                        
                    @endforeach
                </tbody>




            </table>
            
            
            @else
                <div class="alert alert-info">
                    <h4>INFORMACION</h4>no existe ninguna especialidad creada
                </div>
            @endif
            
        </div>
    </div>
</div>
{!! $especialidades->render() !!}
@endsection