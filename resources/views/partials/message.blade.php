@if(Session::has('msg_title'))
    <div class="alert alert-{{ Session::get('msg_type') }}">
        <h4>{{ Session::get('msg_title') }}</h4>{{ Session::get('msg_description') }}
    </div>
@endif