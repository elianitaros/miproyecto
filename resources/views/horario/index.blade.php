@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary btn-xs">NUEVO HORARIO</a>

            @include('partials.message')

            
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>especialidad</th>
                        <th>medico</th>
                        <th>turno</th>
                        <th>dias</th>
                        <th>tiempo consulta</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <a class="btn btn-primary btn-xs" href="{{ url('horary/create') }}">NUEVA horario</a>
                        <tr>
                            <td>{{ $user->people->name }}</td>
                            <td>{{ $user->people->firstname }} {{ $user->people->lastname }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type }}</td>
                            
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('users.show', ['id' => $user->id]) }}" title="Editar usuario">Ver</a>
                                <a class="btn btn-warning btn-xs" href="{{ route('users.edit', ['id' => $user->id]) }}" title="Editar usuario">Editar</a>
                                <a class="btn btn-danger btn-xs" href="{{ route('users.destroy', ['id' => $user->id]) }}" title="Eliminar usuario">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $users->links() !!}
            @else
                <div class="alert alert-info">
                    <h4>INFORMACION</h4>No existe ni un usuario registrado en el sistema.
                </div>
            @endif
        </div>
    </div>
</div>
@endcontent