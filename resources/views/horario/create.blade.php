@extends('layouts.app')
@section('content')
<div class="container">
    {!! Form::open() !!}
    <div class="pull-left">
        <h2>Agrega un nuevo horario</h2>
    </div>
                  @include('datepicker')
                 
                 <div class="row">
                    <div class="col-lg-12">
                        <div class='col-md-5'>
                                    <div class="form-group">
                                            <div class='input-group date' id='birthdate'>
                                                <input type="text" class="form-control datepicker" name="date">
                                                    <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
                                    </div>
                        </div>
                    </div>
                    <script>
                            $('.datepicker').datepicker({
                                    format: "yyyy/mm/dd",
                                            language: "es",
                                                 autoclose: true
                            });
                    </script>
                 </div>
                       


        <div>
            {{ Form::label( 'ESPECIALIDAD') }}
            <div class="row">
                <div class='col-sm-5'>
                    <select name="turno" class="form-control">
                        <option value="" disabled selected>Elija el tiempo que debe durar cada consulta</option>
                        <option value="">pediatria</option>
                        <option value="">medico general</option>
                        <option value="">2</option>
                        <option value="">25 min.</option>
                    </select>
                </div>
            </div>
        </div>
            
        <div>
            {{ Form::label( 'TURNO ') }}
            <div class="row">
                <div class='col-sm-5'>
                    <select name="turno" class="form-control">
                        <option value="" disabled selected>Elige un turno</option>
                        <option value="">mañana</option>
                        <option value="">tarde</option>
                        <option value="">noche</option>
                    </select>                    
                </div>
            </div>
        </div>




        <div class='col-sm-6'>
            {{ Form::label( 'HORA INICIO' ) }}
        </div>
        <div class='col-sm-5'>
                {{ Form::label( 'HORA FIN' ) }}
        </div>

        <div class="container">
                @include('datetimepicker')
                <div class="row">
                    <div class='col-sm-5'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker'>
                                <input class="form-control" type="text" id="time"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script>
                        $('#time').datetimepicker({
                            format: 'LT'
                        });
                    </script>

            
                    <div class='col-sm-5'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker'>
                                <input class="form-control" type="text" id="time2"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script>
                        $('#time2').datetimepicker({
                            format: 'LT'
                        });
                    </script>
                </div>
        </div>

        
        <div>
            

        </div>
        
        <div>
            {{ Form::label( 'TIEMPO DE ATENCION ') }}
            <div class="row">
                <div class='col-sm-5'>        
                    <select name="turno" class="form-control">
                        <option value="" disabled selected>Elija el tiempo que debe durar cada consulta</option>
                        <option value="">10 min.</option>
                        <option value="">15 min.</option>
                        <option value="">20 min.</option>
                        <option value="">25 min.</option>
                    </select>                  
                </div>
            </div>
        </div>

        

        <div class="form-group">
            <a class="btn btn-xs btn-danger" href="{{ url('users') }}">CANCELAR</a>
                <button class="btn btn-success btn-xs">GUARDAR<br></button>
        </div>

    {!! Form::close() !!}
</div>
@endsection
