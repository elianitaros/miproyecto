@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary btn-xs" href="{{ url('users/create') }}">NUEVO USUARIO</a>

            @include('partials.message')

            @if(count($users) > 0)
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>USERNAME</th>
                        <th>EMAIL</th>
                        <th>TIPO DE USUARIO</th>
                        <th>ESPECIALIDAD</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->people->name }}</td>
                            <td>{{ $user->people->firstname }} {{ $user->people->lastname }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type }}</td>
                            <th>{{ $user->medical_speciality_id}}</th>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('users.show', $user->id) }}" title="ver usuario">Ver</a>
                                <a class="btn btn-warning btn-xs" href="{{ route('users.edit', ['id' => $user->id]) }}" title="Editar usuario">Editar</a>
                                <a class="btn btn-danger btn-xs" href="{{ route('users.destroy', ['id' => $user->id]) }}" title="Eliminar usuario">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $users->links() !!}
            @else
                <div class="alert alert-info">
                    <h4>INFORMACION</h4>No existe ni un usuario registrado en el sistema.
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

