@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            {!! Form::model($user, ['route' => ['users.update' => $user], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="form-group">
                    {{ Form::label('name', 'nombres', ['class' => 'text-uppercase']) }}
                    {{ Form::text('name', $user->people->name, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('firstname', 'apellido paterno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('firstname', $user->people->firstname, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('lastname', 'apellido materno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('lastname', $user->people->lastname, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('username', 'nombre de usuario', ['class' => 'text-uppercase']) }}
                    {{ Form::text('username', $user->username, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'E-mail', ['class' => 'text-uppercase']) }}
                    {{ Form::email('email', $user->email, ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Contraseña', ['class' => 'text-uppercase']) }}
                    {{ Form::password('password', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <a class="btn btn-xs btn-danger" href="{{ url('users') }}">CANCELAR</a>
                    <button class="btn btn-success btn-xs">GUARDAR</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

