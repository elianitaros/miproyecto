@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'users.store', 'method' => 'post', 'role' => 'form']) !!}
                @include('partials.errors')
                
                <div class="form-group">
                    {{ Form::label('name', 'nombres', ['class' => 'text-uppercase']) }}
                    {{ Form::text('name', old('name'), ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('firstname', 'apellido paterno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('firstname', old('firsname'), ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('lastname', 'apellido materno', ['class' => 'text-uppercase']) }}
                    {{ Form::text('lastname', old('lastname'), ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('username', 'nombre de usuario', ['class' => 'text-uppercase']) }}
                    {{ Form::text('username', old('username'), ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'E-mail', ['class' => 'text-uppercase']) }}
                    {{ Form::email('email', old('email'), ['class' => 'form-control text-uppercase']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Contraseña', ['class' => 'text-uppercase']) }}
                    {{ Form::password('password', null, ['class' => 'form-control']) }}
                </div>
                <div>
                    {{ Form::label('type', 'TIPO DE USUARIO',['for' => 'type'])}}
                    {{ Form::select('type', array('admin'=>'ADMINISTRADOR', 'medico'=>'MEDICO', 'fichaje'=>'FICHAJE', 'farmaceutica'=>'FARMACEUTICA'),null, ['id' => 'type', 'class' => 'form-control text-uppercase']) }}
                </div>
                <div id="myajax">
                   
                     <input name='medical_speciality_id'/>
                    
                </div>
               
                <div class="form-group">
                    <a class="btn btn-danger" href="{{ url('users') }}">CANCELAR</a>
                    <button class="btn btn-success ">GUARDAR</button>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection


