@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-lg-10an">
                <div class='container-fluid'>
                <div class="btn-group-vertical">
                      <button type="button" class="btn btn-default">PACIENTES</button>
                      <button type="button" class="btn btn-default">TURNOS</button>
                      <button type="button" class="btn btn-primary btn-xs"><img src="{!!asset('images/ii.png')!!}" border='19'><br>MEDICOS</button>
                      <button type="button" class="btn btn-default"><i class="fa fa-user-md" aria-hidden="true"></i>REPORTES</button>
                </div>
                </div>
                 </div>
                 </div>
    <div class="row">
        <div class="col-lg-20an">
            <title>CITAS MEDICAS</title>
            <div class="col-lg-4">
                <div class="input-group">
                  <input type="text" class="form-control">
                  <span class="input-group-btn" >
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"> BUSCAR CITA</button>
                  </span>
                </div>
              </div>
            <a class="btn btn-primary btn-xs" href="{{ url('cita/create') }}">NUEVA CITA MEDICA</a>
            
             
           
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>PACIENTE</th>
                        <th>ESPECIALIDAD</th>
                        <th>MEDICO</th>
                        <th>FECHA</th> 
                        <th>ESTADO</th> 
                    </tr>
                </thead>
               
                                       
                                
            </table>
            
                <a class="btn btn-warning btn-xs" href="{{ url('cita/editar') }}"><span class="glyphicon glyphicon-pencil"></span> EDITAR</a>
                <a class="btn btn-danger btn-xs" href="{{ url('cita/eliminar') }}"><span class="glyphicon glyphicon-remove"></span> ELIMINAR</a>
        </div>
    </div>
</div>
@endsection

