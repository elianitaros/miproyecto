@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => 'users.store', 'method' => 'post', 'role' => 'form']) !!}
            <a class="btn btn-primary btn-xs" href="{{ url('pacient/create') }}">NUEVA AFILIACION</a>
               <div class="col-lg-4">
                <div class="input-group">
                  <input type="text" class="form-control">
                  <span class="input-group-btn" >
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"> BUSCAR PACIENTE</button>
                  </span>
                </div>
              </div>
                <div class="form-group">
                    <a class="btn btn-xs btn-danger" href="{{ url('users') }}">CANCELAR</a>
                    <button class="btn btn-success btn-xs">GUARDAR</button>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

